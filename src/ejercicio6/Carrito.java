/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio6;

import java.util.ArrayList;

/**
 *
 * @author CRISTIAN
 */
public class Carrito {
    private ArrayList<DetallesCarrito> DetallesCarrito;

    public Carrito() {
    }

    public Carrito(ArrayList<DetallesCarrito> DetallesCarrito) {
        this.DetallesCarrito = DetallesCarrito;
    }

    public ArrayList<DetallesCarrito> getDet() {
        return DetallesCarrito;
    }

    public void setDet(ArrayList<DetallesCarrito> DetallesCarrito) {
        this.DetallesCarrito = DetallesCarrito;
    }
    
    
}
