/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio6;

/**
 *
 * @author CRISTIAN
 */
public class DetallesCarrito {
    private Articulos articulos;
    private int cantidad;
    private float precio;

    public DetallesCarrito(Articulos articulos, int cantidad, float precio) {
        this.articulos = articulos;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public Articulos getArticulos() {
        return articulos;
    }

    public void setArticulos(Articulos articulos) {
        this.articulos = articulos;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
}
